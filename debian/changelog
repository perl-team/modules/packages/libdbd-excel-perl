libdbd-excel-perl (0.07-1) unstable; urgency=medium

  * Import upstream version 0.07.
  * Add debian/upstream/metadata.
  * Refresh patches.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Make short description a noun phrase.

 -- gregor herrmann <gregoa@debian.org>  Sun, 18 Feb 2024 01:04:12 +0100

libdbd-excel-perl (0.06-8) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 15:19:45 +0100

libdbd-excel-perl (0.06-7.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 19:23:08 +0100

libdbd-excel-perl (0.06-7) unstable; urgency=medium

  * Team upload

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.
  * Change my email address.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * Use source format 3.0 (quilt); drop README.source and quilt framework.
  * Convert debian/copyright to proposed machine-readable format.
  * Refresh rules for debhelper 7.
  * Fix POD syntax error.
    + new patch: pod.patch
  * Bump Standards-Version to 3.8.4.
  * Update my email address.
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Remove Carlo Segre from Uploaders. Thanks for your work!

  [ Florian Schlichting ]
  * Bump dh compat to level 9
  * Convert d/copyright to copyright-format 1.0
  * Declare compliance with Debian Policy 3.9.8

 -- Florian Schlichting <fsfs@debian.org>  Fri, 25 Nov 2016 21:03:10 +0100

libdbd-excel-perl (0.06-6) unstable; urgency=low

  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * debian/watch: use dist-based URL.
  * debian/rules:
    - delete /usr/lib/perl5 only if it exists (closes: #467830)
    - update with the help of dh-make-perl's templates; don't
      install README anymore (contains only installation instructions)
  * Set Standards-Version to 3.7.3 (no changes).
  * Set debhelper compatibility level to 6.
  * Create new patch perl_path.patch instead of changing Excel.pm directly
    from debian/rules; add quilt framework.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sat, 01 Mar 2008 23:11:07 +0100

libdbd-excel-perl (0.06-5) unstable; urgency=low

  * Make (build) dependency on libspreadsheet-parseexcel-perl versioned
    (closes: #420037).
  * Add dh_md5sums to debian/rules.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Mon, 23 Apr 2007 16:37:34 +0200

libdbd-excel-perl (0.06-4) unstable; urgency=low

  * Moved debhelper to Build-Depends.
  * Set Standards-Version to 3.7.2 (no changes).
  * Set Debhelper Compatibility Level to 5.
  * Removed empty /usr/lib/perl5 from package.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Fri, 16 Jun 2006 15:21:48 +0200

libdbd-excel-perl (0.06-3) unstable; urgency=low

  * Minor typo in description (Closes: #30068)

 -- Gunnar Wolf <gwolf@debian.org>  Mon, 18 Apr 2005 15:01:02 -0500

libdbd-excel-perl (0.06-2) unstable; urgency=low

  * Modified source package name to libole-storage-lite-perl to be
    consistent with Perl package naming scheme
  * New maintainer - Debian Perl Group <pkg-perl-
    maintainer@lists.alioth.debian.org> via Gunnar Wolf
    <gwolf@debian.org>
  * Moved to section: perl
  * Bumped up standards-version to 3.6.1

 -- Gunnar Wolf <gwolf@debian.org>  Tue, 25 Jan 2005 11:13:17 -0600

dbd-excel (0.06-1) unstable; urgency=low

  * New upstream release 0.06

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 13 Nov 2002 20:18:57 -0600

dbd-excel (0.05-3) unstable; urgency=low

  * Makefile.PM: Applied one-character patch to comment out
    'realclean' => '*.xsi' which Perl 5.8 doesn't tolerate (Closes: #165988)

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 24 Oct 2002 21:43:19 -0500

dbd-excel (0.05-2) unstable; urgency=low

  * debian/copyright: Corrected common-license/GPL file ref (lintian)
  * debian/control: Added Depends: to Build-Depends: (Closes: #133427)

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 11 Feb 2002 12:36:03 -0600

dbd-excel (0.05-1) unstable; urgency=low

  * New upstream release 0.05

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 14 Jul 2001 11:18:11 -0500

dbd-excel (0.04-1) unstable; urgency=low

  * New upstream release 0.04

 -- Dirk Eddelbuettel <edd@debian.org>  Fri,  6 Jul 2001 19:37:50 -0500

dbd-excel (0.03-1) unstable; urgency=low

  * New upstream release 0.03

 -- Dirk Eddelbuettel <edd@debian.org>  Wed,  6 Jun 2001 19:32:43 -0500

dbd-excel (0.02-1) unstable; urgency=low

  * Initial Debian release following WNPP heads-up (Closes: #98952)

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 30 May 2001 20:56:23 -0500
